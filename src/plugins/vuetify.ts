import Vue from 'vue';

// Lib
import Vuetify from 'vuetify';
import pt from 'vuetify/src/locale/pt';

// Styles
import 'vuetify/src/stylus/app.styl';

Vue.use(Vuetify, {
  iconfont: 'md',
  lang: {
    locales: { pt },
    current: 'pt',
  },
});
