import Vue from 'vue';

import VueApollo from 'vue-apollo';
import ApolloClient from 'apollo-boost';

export const apolloClient = new ApolloClient({
  uri: process.env.VUE_APP_GRAPHQL_HTTP,
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  defaultOptions: {
    $query: {
      loadingKey: 'loading',
      fetchPolicy: 'cache-and-network',
    },
  },
});

Vue.use(VueApollo);

export default apolloProvider;
