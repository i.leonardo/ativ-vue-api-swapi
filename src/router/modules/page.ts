export interface Children {
  path: string
  name: string
  meta?: {
    icon: string
  }
}

export const children = [
  {
    path: '/home',
    name: 'Home',
    meta: {
      icon: 'home',
    },
    component: () => import(
      /* webpackChunkName: "page" */
      '@/views/page/Home.vue'
    ),
  },
];

export const page = [
  {
    path: '/',
    component: () => import(
      /* webpackChunkName: "root" */
      '@/views/page/Root.vue'
    ),
    redirect: '/home',
    children,
  },
];
