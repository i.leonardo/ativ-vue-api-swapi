import Vue from 'vue';
import Router from 'vue-router';

// data
import { page } from './modules/page';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(_to, _from, savedPosition) {
    if (savedPosition) return savedPosition;
    return { x: 0, y: 0 };
  },
  routes: [
    ...page,
  ],
});
