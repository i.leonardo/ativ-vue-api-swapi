import Vue from 'vue';

export const store = Vue.observable({
  dark: false,
  drawer: false,
  msg: {
    status: false,
    message: '',
    type: '',
  },
});

interface Indexable {
  [key: string]: any;
}

const set = (property: string) => (payload: string|number|boolean|object) => {
  if (property.indexOf('.') > -1) {
    const [index, key] = property.split('.');
    Vue.set((store as Indexable)[index], key, payload);
  } else {
    Vue.set(store, property, payload);
  }
};

const toggle = (property: string) => () => {
  Vue.set(store, property, !(store as Indexable)[property]);
};

export const mutations = {
  toggleDark: toggle('dark'),
  toggleDrawer: toggle('drawer'),
  setDrawer: set('drawer'),
  setMsg: set('msg'),
  boolMsg: set('msg.status'),
};
