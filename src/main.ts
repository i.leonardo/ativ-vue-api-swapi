// Vue
import Vue from 'vue';
import App from './App.vue';

// PLugins
import apolloProvider from './plugins/vue-apollo';
import './plugins/vuetify';
import './registerServiceWorker';

// Config
import router from './router';

// Init
Vue.config.productionTip = false;

new Vue({
  apolloProvider,
  router,
  render: h => h(App),
}).$mount('#app');
