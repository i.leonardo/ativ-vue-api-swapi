import { Vue } from 'vue-property-decorator';

// State Management
import { mutations } from '@/observable';

interface OnError {
  message: string
}

const onMsg = Vue.extend({
  methods: {
    onMsg(type: string, { message }: OnError) {
      mutations.setMsg({
        status: true,
        message,
        type,
      });
    },
  },
});

export default onMsg;
