# SWAPI - GraphQL

> **Typescript** Required - Testing: **v3.4.5**
>
> Site: https://i.leonardo.gitlab.io/ativ-vue-api-swapi

Site **Front-end** para consumir dados da API SWAPI Star Wars com um visual Material Design.

![main](https://gitlab.com/i.leonardo/ativ-vue-api-swapi/raw/master/main.png)

## Requisitos

> - [VueJS](https://vuejs.org/): **v2.6.10**
> - [vue-router](https://github.com/vuejs/vue-router#readme): **v3.0.6**
> - [vuetify](https://github.com/vuetifyjs/vuetify): **v1.5.14** - Framework Material Design
> - [apollo-boost](https://github.com/apollographql/apollo-client#readme): **v0.3.1** - Ferramentas GraphQL Apollo Client
> - [vue-apollo](https://github.com/Akryum/vue-apollo#readme): **v3.0.0-beta.29** - Componente Apollo
> - [vue-class-component](https://github.com/vuejs/vue-class-component#readme): **v7.0.2** - ES201X/TypeScript class decorator for Vue components
> - [vue-property-decorator](https://github.com/kaorun343/vue-property-decorator#readme): **v8.1.0** - Property decorators for Vue Component
> - **Plugins:**
>   - [eslint-plugin-vue](https://github.com/vuejs/eslint-plugin-vue#readme): **v5.2.2** - Eslint + Airbnb
>   - [Vue Observable](https://vuejs.org/v2/api/#Vue-observable): **v2.6.10** - State Management *Required Vue **v2.6.x***
> - **Dependências:**
>   - [@mdi/font](http://materialdesignicons.com/): **link request latest** - Icons Material Design (adicional além do Google - vuetify)
>   - [graphql](https://github.com/graphql/graphql-js): **v14.2.1** - Necessário para o apollo-boost
> - **Dev Dependências:**
>   - [@vue/cli](https://cli.vuejs.org/): **v3.7.0** - Ferramentas Padrão para o Desenvolvimento do Vue.js
>   - [stylus](https://www.npmjs.com/package/stylus): **v0.54.5** - Pré-processador CSS expressivo, dinâmico e robusto
> - **Editor usado:**
>   - Visual Studio Code
>     - [editorconfig.editorconfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig): **EditorConfig for VS Code**
>     - [dbaeumer.vscode-eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint): **ESLint** - [Deixe o ESLint trabalhar para você, no Visual Studio Code](http://vuejs-brasil.com.br/deixe-o-eslint-trabalhar-para-voce-no-visual-studio-code/)

## Project Setup

```bash
# install dependencies
npm install

# Compiles and hot-reloads for development (port 9000)
npm run serve

# Validate or fix code with the rules of the extends: plugin:vue/recommended, @vue/airbnb
npm run lint

# build for production with minification (paste: dist)
npm run build
```

## Árvore (tree)

```
.
|   .browserslistrc
|   .editorconfig
|   .env (Connect Server)
|   .env.development (Vue.js devtools)
|   .eslintrc.js (Rules)
|   .gitignore
|   .gitlab-ci.yml GitLab Pages
|   LICENSE
|   package.json
|   postcss.config.js
|   README.md
|   tsconfig.json (Compiler Options Typescript)
|   vue.config.js (Vue CLI)
|
+---dist (`npm run build`)
+---src (project)
|   |   App.vue (Template)
|   |   main.ts
|   |   shims-gql.d.ts (module *.gql)
|   |   shims-tsx.d.ts (module *.tsx)
|   |   shims-vue.d.ts (module *.vue)
|   |
|   +---assets (media [*.png, *.jpg])
|   +---components
|   +---graphql (Query)
|   +---mixins (Vue.mixin)
|   +---observable (Vue state | mutation)
|   +---plugins
|   |   |   vue-apollo.ts
|   |   \   vuetify.ts
|   |
|   +---router (Vue Router and modules)
|   \---views (Templates vue-router)
|
\---public (template for $mount('#app'))
    |   favicon.ico
    |   index.html (Template)
    \   manifest.json
```

## Agradecimentos

https://swapi.graph.cool/: Fornecimento do SWAPI em GraphQL

[Capas de Filmes HD](http://capadefilmeshd.blogspot.com/): Pelas Imagens das capas de cada episódio

## Licença

> [MIT License](/blob/master/LICENSE)

Copyright © 2019 [Leonardo C. Carvalho](https://gitlab.com/i.leonardo)