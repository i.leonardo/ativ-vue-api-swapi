module.exports = {
  chainWebpack: (config) => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => Object.assign(options, {
        transformAssetUrls: {
          VBtn: 'href',
          VImg: ['src', 'lazy-src'],
          VCard: 'src',
          VCardMedia: 'src',
          VResponsive: 'src',
          VParallax: 'src',
          ApolloMutation: 'mutation',
          ApolloQuery: 'query',
        },
      }));
    config.module
      .rule('graphql')
      .test(/\.gql$/)
      .use('graphql-tag/loader')
      .loader('graphql-tag/loader')
      .end();
  },

  productionSourceMap: false,

  publicPath: process.env.NODE_ENV === 'production'
    ? '/ativ-vue-api-swapi/'
    : '/'
};
